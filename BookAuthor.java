package test_2809;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.Formatter;

public class BookAuthor {
    Connection connection;
    Statement statement;
    PreparedStatement preparedStatement;
    ResultSet resultSet;

    Formatter formatter=new Formatter();
    void createTables()
    {
        try {
            connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor","root","Root@12");
            statement=connection.createStatement();
            statement.execute("Create table Author(authorid int primary key auto_increment ,authorname varchar(20));");
            statement.execute("Create table Book(bookid int primary key auto_increment ,bookname varchar(20),bookprice int,authorid int,foreign key(authorid) references author(authorid) );");



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void insertBookDetails(BufferedReader bufferedReader) throws IOException {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor","root","Root@12");
            preparedStatement= connection.prepareStatement("Insert into book(bookname,bookprice,authorid) values(?,?,?);");
            System.out.println("Enter the name of the book");
            String bookName=bufferedReader.readLine();
            preparedStatement.setString(1,bookName);
            System.out.println("Enter the price of the book");
            int bookPrice=Integer.parseInt(bufferedReader.readLine());
            preparedStatement.setInt(2,bookPrice);
            System.out.println("Enter the authorid who wrote book of the book");
            int authorid=Integer.parseInt(bufferedReader.readLine());
            preparedStatement.setInt(3,authorid);
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void insertAuthorDetails(BufferedReader bufferedReader) throws IOException {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            preparedStatement = connection.prepareStatement("Insert into author(authorname) values(?);");
            System.out.println("Enter the name of the author");
            String authorName = bufferedReader.readLine();
            preparedStatement.setString(1, authorName);
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    void displayAuthor()
    {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("Select * from author;");

            formatter.format("%13s %13s \n","AUTHOR ID","AUTHOR NAME");
            while(resultSet.next()) {

                formatter.format("%13s %13s\n",resultSet.getInt(1),resultSet.getString(2));

            }

            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    void displayBook()
    {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("Select * from book;");

            formatter.format("%13s %13s %13s %13s\n","BOOK ID","BOOK NAME","BOOK PRICE","AUTHOR ID");
            while(resultSet.next()) {

                formatter.format("%13s %13s %13s %13s\n",resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getInt(4));

            }

            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    void query()
    {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            statement=connection.createStatement();
            resultSet= statement.executeQuery("select author.authorname,book.bookname from (author inner join book on author.authorid=book.authorid);");




            formatter.format("%13s %13s\n","AUTHOR NAME","BOOK NAME");
            while(resultSet.next()) {

                formatter.format("%13s %13s \n",resultSet.getString(1),resultSet.getString(2));

            }

            System.out.println(formatter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    void updatePrice(BufferedReader bufferedReader) throws IOException {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            System.out.println("Enter the id of the book you want to increase price");
            int bookid=Integer.parseInt(bufferedReader.readLine());
            System.out.println("Enter the price");
            int updateValue=Integer.parseInt(bufferedReader.readLine());

            preparedStatement= connection.prepareStatement("Update book set bookprice=bookprice+"+updateValue+" where bookid="+bookid+";");
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    void deleteBook(BufferedReader bufferedReader) throws IOException {
            try {
                connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
                System.out.println("Enter the id of the boook you want to delete");
                int bookid=Integer.parseInt(bufferedReader.readLine());
                statement=connection.createStatement();
                statement.execute("delete from book where bookid="+bookid+";");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

    }


    void deleteAuthor(BufferedReader bufferedReader) throws IOException {
        try {
            connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/BookAuthor", "root", "Root@12");
            System.out.println("Enter the id of the author you want to delete");
            int authorid=Integer.parseInt(bufferedReader.readLine());
            statement=connection.createStatement();
            statement.execute("delete from author where authorid="+authorid+";");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}

