package test_2809;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BookAuthorDemo {

    public static void main(String[] args) throws IOException {
        BookAuthor bookAuthor=new BookAuthor();
        Scanner scanner=new Scanner(System.in);
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));



        while(true)
        {
            System.out.println("1 for creating tables");
            System.out.println("2 for Creating and inserting values in Book table");
            System.out.println("3 for Creating and inserting values in Author table");
            System.out.println("4 for Displaying author table");
            System.out.println("5 for Displaying book table");
            System.out.println("6 for Displaying the query");
            System.out.println("7 for updating the price");
            System.out.println("8 for deleting book");
            System.out.println("9 for deleting author");
            System.out.println("Enter the option you want");
            int op= scanner.nextInt();
            switch(op)
            {
                case 1:
                    bookAuthor.createTables();
                    break;

                case 2:
                    bookAuthor.insertBookDetails(bufferedReader);
                    break;

                case 3:
                    bookAuthor.insertAuthorDetails(bufferedReader);
                    break;

                case 4:
                    bookAuthor.displayAuthor();
                    break;

                case 5:
                    bookAuthor.displayBook();
                    break;

                case 6:
                    bookAuthor.query();
                    break;

                case 7:
                    bookAuthor.updatePrice(bufferedReader);
                    break;

                case 8:
                    bookAuthor.deleteBook(bufferedReader);
                    break;

                case 9:
                    bookAuthor.deleteAuthor(bufferedReader);
                    break;

            }
        }




    }
}



/*output
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
2
Enter the name of the book
angular
Enter the price of the book
450
Enter the authorid who wrote book of the book
1
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
3
Enter the name of the author
rt
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
4
    AUTHOR ID   AUTHOR NAME
            1       author1
            2       author2
            3       author4
            4       author5
            5            rt

1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
5
    AUTHOR ID   AUTHOR NAME
            1       author1
            2       author2
            3       author4
            4       author5
            5            rt
      BOOK ID     BOOK NAME    BOOK PRICE     AUTHOR ID
            1         book1          1000             3
            2         book2           100             1
            3         book3           150             1
            4         book4            80             3
            6         book8           800             4
            7       angular           450             1

1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
6
    AUTHOR ID   AUTHOR NAME
            1       author1
            2       author2
            3       author4
            4       author5
            5            rt
      BOOK ID     BOOK NAME    BOOK PRICE     AUTHOR ID
            1         book1          1000             3
            2         book2           100             1
            3         book3           150             1
            4         book4            80             3
            6         book8           800             4
            7       angular           450             1
  AUTHOR NAME     BOOK NAME
      author4         book1
      author1         book2
      author1         book3
      author4         book4
      author5         book8
      author1       angular

1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
7
Enter the id of the book you want to increase price
1
Enter the price
2500
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
8
Enter the id of the boook you want to delete
1
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
Enter the option you want
9
Enter the id of the author you want to delete
9
1 for creating tables
2 for Creating and inserting values in Book table
3 for Creating and inserting values in Author table
4 for Displaying author table
5 for Displaying book table
6 for Displaying the query
7 for updating the price
8 for deleting book
9 for deleting author
 */